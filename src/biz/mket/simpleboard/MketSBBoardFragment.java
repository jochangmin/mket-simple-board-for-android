package biz.mket.simpleboard;

import java.util.ArrayList;
import java.util.Random;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import biz.mket.image_cache.MketImageFetcher;
import biz.mket.simpleboard.article.MketSBArticle;
import biz.mket.simpleboard.article.MketSBBoardManager;
import biz.mket.simpleboard.settings.SimpleBoardSetting;
import biz.mket.tools.MketMemoryUtils;

import com.handmark.pulltorefresh.library.PullToRefreshBase;
import com.handmark.pulltorefresh.library.PullToRefreshBase.OnRefreshListener2;
import com.handmark.pulltorefresh.library.PullToRefreshListView;

public class MketSBBoardFragment extends Fragment implements OnRefreshListener2 {

	// it's used for the key when saving an article on the Simple Board Cloud
	// Server (SBC)
	// for example, "tv.showket.product_comment"
	
	public static final String EXTRA_BOARD_KEY = "extra_key";

	protected static final int _REQCODE_WRITE = 16784;
	protected static final int _INIT_ARTICLE_COUNT = 10;

	// Instance Members
	protected String _boardKey;
	protected SimpleBoardSetting _settings;

	protected MketSBInterface _boardInterface;
	// protected int _maximumContentLength;
	// protected int _maximumImageCount;

	protected PullToRefreshListView _rootListView;

	protected MketSBBoardManager _boardManager;
	protected MketImageFetcher _fetcher;
	protected _ArticlesAdapter _adapter;

	protected int _userImageSize;
	protected int _previewSize;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		// Get Arguments
		Bundle args = getArguments();
		_boardKey = args.getString(EXTRA_BOARD_KEY);
		
		// Board Manager
		_settings = SimpleBoardSetting.getSetting(_boardKey);
		Log.d("SIMPLE_BOARD", "MketSBBoardFragment.onCreate _boardKey: "+ _boardKey);
		Log.d("SIMPLE_BOARD", "MketSBBoardFragment.onCreate _settings: "+ _settings);
		Log.d("SIMPLE_BOARD", "MketSBBoardFragment.onCreate _settings.key: "+ _settings.key);
		Log.d("SIMPLE_BOARD", "MketSBBoardFragment.onCreate _settings.article_url: "+ _settings.article_url);
		Log.d("SIMPLE_BOARD", "MketSBBoardFragment.onCreate _settings.comment_url: "+ _settings.comment_url);

		// Set Interface
		_boardInterface = getInterface();

		// Set the Image Fetcher
		_fetcher = _boardInterface.getImageFetcher();

		// Set Board Manager (for communicating with the server)
		_boardManager = MketSBBoardManager.singleton(getActivity(), _settings.article_url);

		// Set Screen Size
		Display display = getActivity().getWindowManager().getDefaultDisplay();
		_previewSize = (int) (display.getWidth() * 0.8f); // deprecated

		// _maximumContentLength = args.getInt(EXTRA_MAXIMUM_CONTENT_LEGNTH,
		// -1);
		// _maximumImageCount = args.getInt(EXTRA_MAXIMUM_IMAGE_COUNT, -1);

		// _mketUser =
		// MketUser.getSingleton(getActivity().getApplicationContext());
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		super.onCreateView(inflater, container, savedInstanceState);

		_rootListView = initPullToRefreshListView(inflater, container, savedInstanceState);
		_rootListView.setOnRefreshListener(this);
		// _rootListView.setClickable(false);
		// _rootListView.setFocusable(false);
		// _rootListView.setFocusableInTouchMode(false);
		// _rootListView.setSelected(false);
		// _rootListView.getRefreshableView().setClickable(false);
		// _rootListView.getRefreshableView().setFocusable(false);
		// _rootListView.getRefreshableView().setFocusableInTouchMode(false);
		// _rootListView.getRefreshableView().setSelected(false);

		// init Header View
		View headerView = initHeader(inflater, container, savedInstanceState);
		_rootListView.getRefreshableView().addHeaderView(headerView);
		
		// Set Colors
		if(_settings.background != null){
			_rootListView.setBackgroundDrawable(_settings.background);
		}
		

		// init Pull To Refresh
		initBoard();

		return _rootListView;
	}
	
	public PullToRefreshListView initPullToRefreshListView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){
		return (PullToRefreshListView) inflater.inflate(R.layout.simpleboard_list_layout, null);
	}

	public View initHeader(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		LinearLayout headerView = (LinearLayout) inflater.inflate(R.layout.simpleboard_list_header, null);
		Button writeButtonView = (Button) headerView.findViewById(R.id.mket_simple_board_write_button);
		
		if(_settings.writable){
			writeButtonView.setBackgroundResource(_settings.write_background);			
			
			// listen Write Button
			writeButtonView.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					writeArticle();
				}
			});			
		}
		else{
			writeButtonView.setVisibility(View.GONE);
		}
		
		return headerView;
		
	}

	public void initBoard() {
		// init Adapter
		_adapter = new _ArticlesAdapter();
		_rootListView.setAdapter(_adapter);
		retrieveArticles(false);

		// _boardManager.listenRetrieveArticles(new MketHttpCallback() {
		// @Override
		// public void callback(MketHttpClientResponse clientResponse) {
		// _rootListView.onRefreshComplete();
		// }
		// });
		// _boardManager.retrieveArticlesFromServer(_boardKey, 0,
		// _INIT_ARTICLE_COUNT,
		// true);
	}

	public void retrieveArticles(boolean refresh) {
		_boardManager.retrieveArticlesFromServer(_boardKey, null, 1, null, refresh, new Runnable() {
			@Override
			public void run() {
				_rootListView.onRefreshComplete();
				_adapter.notifyDataSetChanged();

			}
		});
	}

	@Override
	public void onResume() {
		super.onResume();
		// _mketUser.simpleLogin();

	}

	@Override
	public void onPause() {
		super.onPause();
	}

	@Override
	public void onStart() {
		super.onStart();

	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		MketMemoryUtils.recursiveRecycle(_rootListView, false);
		Log.d("CM", "MketSimpleBoardFragment.onDestroy --> xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx");
		
		if (_fetcher != null) {
			_fetcher.flushCacheAsynchronously();
			_fetcher.closeCacheAsynchronously();
			_fetcher = null;
		}

		if (_adapter != null) {
			_adapter = null;
		}
		
		
		if(_boardManager != null){
			_boardManager.destory();
			_boardManager = null;
		}

		_boardKey = null;
		_settings = null;

		_rootListView = null;
		_boardInterface = null;
		_fetcher = null;
		_adapter = null;

		_userImageSize = 0;
		_previewSize = 0;
	}

	@SuppressWarnings("rawtypes")
	@Override
	public void onPullDownToRefresh(PullToRefreshBase refreshView) {
		Log.d("CM", "onPullDownToRefresh");
		retrieveArticles(true);

	}
	
	@SuppressWarnings("rawtypes")
	@Override
	public void onPullUpToRefresh(PullToRefreshBase refreshView) {
		Log.d("CM", "onPullUpToRefresh");
		retrieveArticles(true);
	}
	
	
	public MketSBInterface getInterface(){
		return (MketSBInterface) getActivity();
	}

	public void writeArticle() {
		if (!_boardInterface.is_authenticated()) {
			_boardInterface.login();
		} else {
			Intent writeIntent = new Intent(getActivity(), MketSBWriteActivity.class);
			writeIntent.putExtra(MketSBWriteActivity.EXTRA_BOARD_KEY, _boardKey);
			startActivityForResult(writeIntent, _REQCODE_WRITE);
		}
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent intentData) {
		super.onActivityResult(requestCode, resultCode, intentData);

		if (requestCode == _REQCODE_WRITE && resultCode == Activity.RESULT_OK) {
			String content = intentData.getStringExtra(MketSBWriteActivity.RETURN_CONTENT);
			ArrayList<String> imagePaths = intentData
					.getStringArrayListExtra(MketSBWriteActivity.RETURN_IMAGES);
			ArrayList<String> videoPaths = intentData
					.getStringArrayListExtra(MketSBWriteActivity.RETURN_VIDEOS);

			_boardInterface.on_write_done(content, (String[]) imagePaths.toArray(), (String[]) videoPaths.toArray());
		}
	}

	protected class _ArticlesAdapter extends BaseAdapter {

		@Override
		public int getCount() {
			if(_boardManager != null && _boardManager.articles != null){
				return _boardManager.articles.size();				
			}
			return 0;
		}

		@Override
		public MketSBArticle getItem(int position) {
			if(_boardManager != null && _boardManager.articles != null){
				return _boardManager.articles.get(position);				
			}
			return null;
		}

		@Override
		public long getItemId(int position) {
			return position;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			final MketSBArticle article = getItem(position);
			Log.d("CM", "_ArticlesAdapter.getView 01");

			if (_fetcher == null || article == null) {
				_fetcher = _boardInterface.getImageFetcher();
			}
			
			convertView = (LinearLayout) LayoutInflater.from(getActivity()).inflate(R.layout.simpleboard_list_item,
					null);

			LinearLayout userContainerView;
			LinearLayout contentContainerView;
			ImageView userImageView;
			ImageView previewView;
			TextView contentView;
			TextView nameView;
			TextView agoView;
			TextView replyCountView;
			TextView likeCountView;
			TextView viewCountView;
			TextView videoStatusView;

			userContainerView = (LinearLayout) convertView.findViewById(R.id.simpleboard_item_user_container);
			contentContainerView = (LinearLayout) convertView.findViewById(R.id.simpleboard_item_content_container);
			contentView = (TextView) convertView.findViewById(R.id.simpleboard_item_content);
			userImageView = (ImageView) convertView.findViewById(R.id.simpleboard_item_user_image);
			nameView = (TextView) convertView.findViewById(R.id.simpleboard_item_name);
			agoView = (TextView) convertView.findViewById(R.id.simpleboard_item_ago);
			replyCountView = (TextView) convertView.findViewById(R.id.simpleboard_item_reply_count);
			likeCountView = (TextView) convertView.findViewById(R.id.simpleboard_item_like_count);
			viewCountView = (TextView) convertView.findViewById(R.id.simpleboard_item_view_count);
			
			
			videoStatusView = (TextView) convertView.findViewById(R.id.simpleboard_item_video_status);
			previewView = (ImageView) convertView.findViewById(R.id.simpleboard_item_preview);

			Log.d("CM", "_ArticlesAdapter.getView 02");

			// Set Title and Content
			int end = 100;
			if (article.content.length() < end) {
				end = article.content.length();
			}
			contentView.setText(article.content.subSequence(0, end));
			nameView.setText(article.authorName);

			// Set Ago (Time)
			agoView.setText(article.ago());

			// Set various Counts
			likeCountView.setText(String.valueOf(article.likeCount));
			replyCountView.setText(String.valueOf(article.commentCount));
			viewCountView.setText(String.valueOf(article.viewCount));

			// Set User Image
			if (article.authorThumbnail != null) {
				_fetcher.loadImage(article.authorThumbnail, userImageView, _userImageSize, _userImageSize, true, true);
			} else {
				Random random = new Random();
				int faceNumber = random.nextInt(4);
				if (faceNumber == 0) {
					faceNumber = R.drawable.sbc_face1;
				} else if (faceNumber == 1) {
					faceNumber = R.drawable.sbc_face2;
				} else if (faceNumber == 2) {
					faceNumber = R.drawable.sbc_face3;
				} else if (faceNumber == 3) {
					faceNumber = R.drawable.sbc_face4;
				}
				userImageView.setImageDrawable(getResources().getDrawable(faceNumber));

			}
			Log.d("CM", "_ArticlesAdapter.getView 03");

			// Set Preview Image
			if (article.preview != null) {
				_fetcher.loadImage(article.preview, previewView, _previewSize, _previewSize, true, true);
			}
			previewView.setMinimumHeight(_userImageSize);
			

			// listen clicking User Container
			userImageView.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					if(_boardInterface != null){						
						_boardInterface.user_activity();
					}
				}
			});

			// listen clicking Content Container
			contentContainerView.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					if(article != null){
						_boardInterface.startDetailActivity(article);
					}
				}
			});
			Log.d("CM", "_ArticlesAdapter.getView 04");

			return convertView;
		}
	}

}