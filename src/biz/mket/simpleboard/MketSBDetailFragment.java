package biz.mket.simpleboard;

import java.util.HashMap;
import java.util.Random;

import android.R.integer;
import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Point;
import android.graphics.Rect;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.WindowManager;
import android.widget.AbsListView;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import biz.mket.datetime.TimeTools;
import biz.mket.image_cache.MketImageCacheCallback;
import biz.mket.image_cache.MketImageFetcher;
import biz.mket.simpleboard.article.MketSBArticle;
import biz.mket.simpleboard.article.MketSBBoardManager;
import biz.mket.simpleboard.comment.MketSBComment;
import biz.mket.simpleboard.comment.MketSBCommentManager;
import biz.mket.simpleboard.comment.MketSBCommentManager.CommentRetrieveCallback;
import biz.mket.simpleboard.settings.SimpleBoardSetting;

import com.handmark.pulltorefresh.library.PullToRefreshBase;
import com.handmark.pulltorefresh.library.PullToRefreshBase.OnRefreshListener2;
import com.handmark.pulltorefresh.library.PullToRefreshListView;

public class MketSBDetailFragment extends Fragment implements OnRefreshListener2 {
	// Extra Members
	public static final String EXTRA_BOARD_KEY = "board_key";
	public static final String EXTRA_ARTICLE_SERVER_ID = "article_server_id";

	// Instance Members
	protected String _boardKey;
	protected String _serverId;

	protected SimpleBoardSetting _settings;

	protected LinearLayout _rootView;
	protected PullToRefreshListView _ptrView;
	protected LinearLayout _commentContainerView;
	protected EditText _commentView;
	protected TextView _sendCommentView;

	protected MketSBBoardManager _boardManager;
	protected MketSBCommentManager _commentManager;
	protected MketSBInterface _boardInterface;
	protected MketSBArticle _article;
	protected MketImageFetcher _fetcher;

	protected _ArticleContentAdapter _adapter;
	protected LayoutInflater _layoutInflater;

	protected int _rootHeight;
	protected int _userImageSize;
	protected int _imageSize;
	protected int _currentPage = 1;
	protected HashMap<String, Integer> _imageHeights;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		Log.d("SIMPLEBOARD", "MketSBDetailFragment.onCreate Start");

		// receive Extra Arguments
		Bundle extraBundle = getArguments();
		_boardKey = extraBundle.getString(EXTRA_BOARD_KEY);
		_serverId = extraBundle.getString(EXTRA_ARTICLE_SERVER_ID);

		// Board Settings
		_settings = SimpleBoardSetting.getSetting(_boardKey);

		// Set Board Interface
		_boardInterface = (MketSBInterface) getActivity();

		// Set Manager and Article
		_boardManager = MketSBBoardManager.singleton(getActivity(), _settings.article_url);
		_commentManager = new MketSBCommentManager(getActivity(), _settings.comment_url);

		// Increase View Count
		_boardManager.increaseViewCount(_serverId);

		// Set Article
		_article = _boardManager.getArticle(_serverId);

		// Set Layout Inflator
		_layoutInflater = LayoutInflater.from(getActivity());

		// Set Image Fetcher
		_fetcher = _boardInterface.getImageFetcher();

		// Set Image Size
		_imageHeights = new HashMap<>();
		_imageSize = (int) getWindowSize().x;

		// Get Detailed Article
		getDetailedArticle();
		Log.d("SIMPLEBOARD", "MketSBDetailFragment.onCreate End");
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		Log.d("SIMPLEBOARD", "MketSBDetailFragment.onCreateView Start");
		super.onCreateView(inflater, container, savedInstanceState);
		_rootView = (LinearLayout) inflater.inflate(R.layout.simpleboard_detail_fragment, null);
		_ptrView = (PullToRefreshListView) _rootView.findViewById(R.id.simpleboard_detail_ptr);
		_ptrView.setOnRefreshListener(this);
		_commentContainerView = (LinearLayout) _rootView.findViewById(R.id.simpleboard_detail_comment_container);
		_commentView = (EditText) _rootView.findViewById(R.id.simpleboard_detail_comment_content);
		_sendCommentView = (TextView) _rootView.findViewById(R.id.simpleboard_detail_send_comment);

		// init Adapter
		_adapter = new _ArticleContentAdapter();
		_ptrView.setAdapter(_adapter);

		// listen Comment Send
		_sendCommentView.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				if (!_boardInterface.is_authenticated()) {
					_boardInterface.login();
					return;
				}
				String comment = _commentView.getText().toString();
				_commentManager.send(_article.serverId, comment, _boardInterface.get_user_id(), new Runnable() {

					@Override
					public void run() {
						getCommentArticles(1, true);
						_boardManager.increaseCommentCount(_article.serverId);
					}
				});
				_commentView.setText(null);
			}
		});

		Log.d("SIMPLEBOARD", "MketSBDetailFragment.onCreateView End");
		return _rootView;
	}

	public int getKeyboardHeight() {
		Rect r = new Rect();
		View decorView = getActivity().getWindow().getDecorView();
		decorView.getWindowVisibleDisplayFrame(r);

		int screenHeight = decorView.getRootView().getHeight();
		int heightDifference = screenHeight - (r.bottom - r.top);

		return heightDifference;
	}

	@Override
	public void onResume() {
		super.onResume();

	}

	@Override
	public void onPullDownToRefresh(PullToRefreshBase refreshView) {
		getCommentArticles(1, false);
	}

	@Override
	public void onPullUpToRefresh(PullToRefreshBase refreshView) {
		getCommentArticles(_currentPage + 1, false);
	}

	public void getDetailedArticle() {
		Log.d("SIMPLEBOARD", "MketSBDetailFragment.getDetailedArticle Start");
		Log.d("SIMPLEBOARD", "MketSBDetailFragment.getDetailedArticle _serverId:" + _serverId);
		if (_serverId != null) {
			_boardManager.retrieveArticlesFromServer(_boardKey, _serverId, new Runnable() {
				@Override
				public void run() {
					if (_ptrView == null) {
						return;
					}
					_ptrView.onRefreshComplete();
					if (_boardManager != null && _serverId != null) {
						_article = _boardManager.getArticle(_serverId);
					}
					getCommentArticles(_currentPage, false);
					_adapter.notifyDataSetChanged();

					Log.d("SIMPLEBOARD", "MketSBDetailFragment.getDetailedArticle callback _article:" + _article);
					Log.d("SIMPLEBOARD", "MketSBDetailFragment.getDetailedArticle callback _article.boardKey:" + _article.boardKey);
					Log.d("SIMPLEBOARD", "MketSBDetailFragment.getDetailedArticle callback _article.serverId:" + _article.serverId);

				}
			});
		}
	}

	public void getCommentArticles(int page, boolean atFirst) {
		_currentPage = page;
		if (_commentManager != null) {
			_commentManager.retrieveComments(_article.serverId, page, atFirst, new CommentRetrieveCallback() {

				@Override
				public void callback(int page, boolean changed) {
					if (_ptrView != null) {
						_ptrView.onRefreshComplete();
						if (changed) {
							_adapter.notifyDataSetChanged();
						}
					}
				}
			});
		}

	}

	@Override
	public void onDestroy() {
		super.onDestroy();

		_boardKey = null;
		_serverId = null;
		_settings = null;

		_ptrView = null;
		_boardManager = null;

		// Instance Members

		_boardInterface = null;
		;
		_article = null;
		;
		_fetcher = null;
		;

		_adapter = null;
		_layoutInflater = null;

		if (_imageHeights != null) {
			_imageHeights.clear();
			_imageHeights = null;
		}

	}

	protected class _ArticleContentAdapter extends BaseAdapter {
		protected static final int TYPE_ARTICLE = 0;
		protected static final int TYPE_IMAGES = 1;
		protected static final int TYPE_COMMENTS = 2;

		protected AbsListView.LayoutParams _imageLayoutParams;

		public _ArticleContentAdapter() {
			_imageLayoutParams = new AbsListView.LayoutParams((int) LayoutParams.MATCH_PARENT, (int) LayoutParams.WRAP_CONTENT);
		}

		@Override
		public int getViewTypeCount() {
			return 3;
		}

		@Override
		public boolean areAllItemsEnabled() {
			return true;
		}

		@Override
		public boolean isEnabled(int position) {
			return true;
		}

		@Override
		public int getCount() {
			if (_article == null) {
				return 0;
			}

			int articleCount = 1;
			int imageCount = _article.images.length;
			int commentCount = _commentManager.comments.size();

			return articleCount + imageCount + commentCount;
		}

		@Override
		public Object getItem(int position) {
			int imageCount = _article.images.length;
			if (position <= imageCount) {
				return _article;
			}
			Log.d("CM", "getItem _commentManager.comments.size() :" + _commentManager.comments.size());
			Log.d("CM", "getItem position :" + position);
			Log.d("CM", "getItem imageCount :" + imageCount);
			Log.d("CM", "getItem position - imageCount :" + (position - imageCount));
			return _commentManager.comments.get(position - imageCount - 1);
		}

		public int getItemViewType(int position) {
			int articleCount = 1;
			int imageCount = _article.images.length;
			int commentCount = _commentManager.comments.size();

			if (position == 0) {
				return TYPE_ARTICLE;
			} else if (position <= imageCount) {
				return TYPE_IMAGES;
			} else if (position <= imageCount + commentCount) {
				return TYPE_COMMENTS;
			}
			return TYPE_ARTICLE;

		};

		@Override
		public long getItemId(int position) {
			// int imageSize = _articleData.authorImage.size();
			// if (position < imageSize) {
			// return 0;
			// } else if (position == imageSize) {
			// return 1;
			// }
			return position;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			int viewType = getItemViewType(position);

			// if (_fetcher == null) {
			// initImageFetcher();
			// }

			if (viewType == TYPE_ARTICLE) {
				// processing Article
				convertView = _processArticle(position, convertView);
			} else if (viewType == TYPE_IMAGES) {
				// processing Images
				convertView = _processImages(position, convertView);
			} else {
				// processing comments
				convertView = _processComment(position, convertView);
			}

			return convertView;
		}

		protected View _processArticle(int position, View convertView) {
			ImageView userImageView;
			TextView nameView;
			TextView agoView;
			TextView replyCountView;
			TextView likeCountView;
			TextView viewCountView;
			TextView websiteView;

			TextView contentView;

			convertView = (LinearLayout) _layoutInflater.inflate(R.layout.simpleboard_detail_article_section, null);

			userImageView = (ImageView) convertView.findViewById(R.id.mketsb_detail_user_image);
			nameView = (TextView) convertView.findViewById(R.id.mketsb_detail_name);
			agoView = (TextView) convertView.findViewById(R.id.mketsb_detail_ago);
			replyCountView = (TextView) convertView.findViewById(R.id.mketsb_detail_reply_count);
			likeCountView = (TextView) convertView.findViewById(R.id.mketsb_detail_like_count);
			viewCountView = (TextView) convertView.findViewById(R.id.mketsb_detail_view_count);

			contentView = (TextView) convertView.findViewById(R.id.mketsb_detail_content);
			websiteView = (TextView) convertView.findViewById(R.id.mketsb_detail_website);

			// Set User
			nameView.setText(_article.authorName);

			// Set User Image
			if (_article.authorThumbnail != null) {
				_fetcher.loadImage(_article.authorThumbnail, userImageView, _userImageSize, _userImageSize, true, true);
			} else {
				Random random = new Random();
				int faceNumber = random.nextInt(4);
				if (faceNumber == 0) {
					faceNumber = R.drawable.sbc_face1;
				} else if (faceNumber == 1) {
					faceNumber = R.drawable.sbc_face2;
				} else if (faceNumber == 2) {
					faceNumber = R.drawable.sbc_face3;
				} else if (faceNumber == 3) {
					faceNumber = R.drawable.sbc_face4;
				}
				userImageView.setImageDrawable(getResources().getDrawable(faceNumber));
			}

			// Set Ago (Time)
			agoView.setText(_article.ago());

			// Set various Counts
			likeCountView.setText(String.valueOf(_article.likeCount));
			replyCountView.setText(String.valueOf(_article.commentCount));
			viewCountView.setText(String.valueOf(_article.viewCount));

			// Set Content
			contentView.setText(_article.content);

			return convertView;
		}

		protected View _processImages(int position, View convertView) {
			final ImageView imageView;

			convertView = (FrameLayout) _layoutInflater.inflate(R.layout.simpleboard_detail_image_section, null);
			imageView = (ImageView) convertView.findViewById(R.id.simpleboard_detail_content_image);

			String imageUrl = _article.images[position - 1];
			
			String imageKey = _fetcher.loadImage(imageUrl, imageView, _imageSize, -1, -1, true, true, _imageHeights, null);

			if (_imageHeights.containsKey(imageKey)) {
				int height = _imageHeights.get(imageKey);
				imageView.getLayoutParams().height = height;
			}

			return convertView;
		}

		protected View _processComment(int position, View convertView) {
			MketSBComment comment = (MketSBComment) getItem(position);

			LinearLayout rootView = (LinearLayout) _layoutInflater.inflate(R.layout.simpleboard_detail_comment, null);
			TextView nameView = (TextView) rootView.findViewById(R.id.simpleboard_detail_comment_author_name);
			TextView contentView = (TextView) rootView.findViewById(R.id.simpleboard_detail_comment_content);
			TextView dateView = (TextView) rootView.findViewById(R.id.simpleboard_detail_comment_date);

			nameView.setText(comment.authorName);
			contentView.setText(comment.content);
			dateView.setText(TimeTools.ago(comment.registered));

			return rootView;
		}

		public void clear() {

		}
	}

	@SuppressLint("NewApi")
	public Point getWindowSize() {
		WindowManager windowManager = (WindowManager) getActivity().getSystemService(Context.WINDOW_SERVICE);

		Display display = windowManager.getDefaultDisplay();
		Point windowSize = new Point();

		if (Build.VERSION.SDK_INT > Build.VERSION_CODES.HONEYCOMB) {
			windowSize = new Point();
			display.getSize(windowSize);
		} else {
			int width = display.getWidth(); // deprecated
			int height = display.getHeight(); // deprecated
			windowSize = new Point();
			windowSize.set(width, height);
		}

		return windowSize;
	};

}
