package biz.mket.simpleboard.comment;

import java.util.Date;

import org.json.JSONException;
import org.json.JSONObject;

import biz.mket.datetime.TimeTools;

public class MketSBComment {
	
	public String serverId;
	public String content;
	public String authorId;
	public String authorName;  
	public Date registered;
	
	
	
	public static MketSBComment create(JSONObject comment){
		MketSBComment commentInstance = new MketSBComment();
		
		try {
			commentInstance.serverId = comment.getString("id");
			commentInstance.authorId = comment.getString("author_id");
			commentInstance.authorName = comment.getString("author_name");
			commentInstance.content = comment.getString("content");
			commentInstance.registered = TimeTools.pythonUtc(comment.getString("registered"));
		} catch (JSONException e) {
			e.printStackTrace();
			return null;
		}
		
		return commentInstance;
	}

}
