package biz.mket.simpleboard.comment;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import biz.mket.http.MketAsyncHttp;
import biz.mket.http.MketHttpCallback;
import biz.mket.http.MketHttpClientResponse;
import biz.mket.simpleboard.article.MketSBBoardManager;
import biz.mket.tools.MketTools;
import biz.mket.tracker.MketTracker;

public class MketSBCommentManager {
	public static interface CommentRetrieveCallback {
		public void callback(int page, boolean changed);
	}

	// Instance Members
	public Context _context;
	public String _commentUrl;
	public ArrayList<MketSBComment> comments;

	public MketSBCommentManager(Context context, String url) {
		_context = context;
		_commentUrl = url;
		comments = new ArrayList<>();

	}

	public void retrieveComments(String articleId, int page, final boolean atFirst, final CommentRetrieveCallback callback) {

		MketAsyncHttp mketHttp = new MketAsyncHttp();
		mketHttp.addQueryString("article", articleId);
		mketHttp.addQueryString("page", page);

		mketHttp.listenPostExecute(new MketHttpCallback() {
			@Override
			public void callback(MketHttpClientResponse clientResponse) {
				
				MketTracker mketTracker = MketTracker.getSingleton();
				mketTracker.send("Mket Simple Board - Retrieve Comments", clientResponse);
				
				boolean changed = false;
				int page = -1;

				try {
					JSONObject json = new JSONObject(clientResponse.content);
					JSONArray commentJson = json.getJSONArray("comments");
					page = json.getInt("page");

					int commentSize = commentJson.length();
					for (int i = 0; i < commentSize; i++) {
						boolean exists = false;
						MketSBComment comment = MketSBComment.create(commentJson.getJSONObject(i));

						for (MketSBComment existingComment : comments) {
							if (existingComment.serverId.equals(comment.serverId)) {
								exists = true;
								break;
							}

						}
						if (exists) {
							continue;
						}

						if (comment != null) {
							if (atFirst) {
								comments.add(0, comment);
							} else {
								comments.add(comment);
							}
							changed = true;
						}
					}
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

				if (callback != null) {
					callback.callback(page, changed);
				}
			}
		});
		mketHttp.sendAsynchronously(_commentUrl);

	}

	public void send(String articleId, String comment, String userId, final Runnable calback) {
		MketAsyncHttp mketHttp = new MketAsyncHttp(MketAsyncHttp.METHOD_POST);
		mketHttp.addNameValue("article", articleId);
		mketHttp.addNameValue("content", comment);
		mketHttp.addNameValue("user", userId);
		mketHttp.addHeader("X-CSRFToken", MketAsyncHttp.getCsrfToken());
		mketHttp.listenPostExecute(new MketHttpCallback() {

			@Override
			public void callback(MketHttpClientResponse clientResponse) {
				MketTracker mketTracker = MketTracker.getSingleton();
				mketTracker.send("Mket Simple Board - Submit Comments", clientResponse);

				if (calback != null) {
					calback.run();
				}
			}
		});
		mketHttp.sendAsynchronously(_commentUrl);

	}

}
