package biz.mket.simpleboard.article;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.util.Log;
import biz.mket.datetime.TimeTools;

public class MketSBArticle {

	// Instance Members
	public String serverId = null;
	public String boardKey = null;
	public String title = null;
	public String content = null;

	public String authorServerId = null;
	public String authorName = null;
	public String authorImage = null;
	public String authorThumbnail = null;

	public int commentCount = 0;
	public int likeCount = 0;
	public int viewCount = 0;

	public Date registered;

	public String image = null;;
	public String preview = null;

	public String[] images;
	public String[] previews;

	// Static Methods
	public static MketSBArticle create(JSONObject json) {
		MketSBArticle article = new MketSBArticle();
		try {
			article.serverId = json.getString("id");
			article.boardKey = json.getString("key");
			article.title = json.getString("title");
			article.content = json.getString("content");

			article.authorServerId = json.getString("author_id");
			article.authorName = json.getString("author_name");
			if (json.has("author_image")) {
				article.authorImage = json.getString("author_image");
			}
			if (json.has("author_thumbnail")) {
				article.authorThumbnail = json.getString("author_thumbnail");
			}

			JSONArray jsonImages = json.getJSONArray("images");
			JSONArray jsonPreviews = json.getJSONArray("previews");
			article.images = new String[jsonImages.length()];
			article.previews = new String[jsonPreviews.length()];

			for (int i = 0; i < jsonImages.length(); i++) {
				article.images[i] = jsonImages.getString(i);
			}
			for (int i = 0; i < jsonPreviews.length(); i++) {
				article.previews[i] = jsonPreviews.getString(i);
			}

			if (article.images.length >= 1) {
				article.image = article.images[0];
			}
			if (article.previews.length >= 1) {
				article.preview = article.previews[0];
			}

			article.commentCount = json.getInt("comment_count");
			article.likeCount = json.getInt("like_count");
			article.viewCount = json.getInt("view_count");

			// Get Times
			// "2014-05-22 12:26:57+00:00"
			
			article.registered = TimeTools.pythonUtc(json.getString("registered"));
//			Calendar gmtCal = Calendar.getInstance(TimeZone.getTimeZone("GMT"));
//			gmtCal.setTimeInMillis(json.getLong("registered") * 1000);
//			article.registered = gmtCal.getTime();

		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
		return article;
	}
	
	public void update(MketSBArticle anotherArticle){
		String tempBoardKey = boardKey; 
		String tempServerId = serverId;
		destroy();
		
		this.boardKey = tempBoardKey;
		this.serverId = tempServerId;
		this.title = anotherArticle.title;
		this.content = anotherArticle.content;
		
		this.authorServerId = anotherArticle.authorServerId;
		this.authorName = anotherArticle.authorName;
		this.authorImage = anotherArticle.authorImage;
		this.authorThumbnail = anotherArticle.authorThumbnail;
		
		this.commentCount = anotherArticle.commentCount;
		this.likeCount = anotherArticle.likeCount;
		this.viewCount = anotherArticle.viewCount;
		
		this.registered = (Date) anotherArticle.registered.clone();
		
		this.image = anotherArticle.image;
		this.preview = anotherArticle.preview;
		this.images = anotherArticle.images.clone();
		this.previews = anotherArticle.previews.clone();
	}

	public void destroy() {
		if (images != null) {
			for (int i = 0; i < images.length; i++) {
				images[i] = null;
			}
			images = null;
		}
		if (previews != null) {
			for (int i = 0; i < previews.length; i++) {
				previews[i] = null;
			}
			previews = null;
		}

		serverId = null;
		boardKey = null;
		title = null;
		content = null;

		authorServerId = null;
		authorName = null;
		authorThumbnail = null;
		authorImage = null;
		authorThumbnail = null;

		commentCount = 0;
		likeCount = 0;
		viewCount = 0;

		image = null;

		registered = null;
	}

	public String ago() {
		long seconds = ((new Date()).getTime() - registered.getTime()) / 1000l;
		String response = "";
		if (seconds < 60) {
			response = seconds + "초전";
		} else if (seconds < 3600) {
			response = seconds / 60 + "분전";
		} else if (seconds < 86400) {
			response = seconds / 3600 + "시간전";
		} else if (seconds < 2678400000l) {
			// 86400000 is a day in miliseconds
			response = seconds / 86400 + "일전";
		} else {
			SimpleDateFormat sdf = new SimpleDateFormat("yy년 MM월 dd일 HH시 mm분");
			response = sdf.format(registered);
		}
		return response;
	}

}
