package biz.mket.simpleboard.article;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map.Entry;

import org.apache.http.HttpStatus;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.util.Log;
import biz.mket.http.MketAsyncHttp;
import biz.mket.http.MketHttpCallback;
import biz.mket.http.MketHttpClientResponse;
import biz.mket.tools.MketTools;
import biz.mket.tracker.MketTracker;

/**
 * 
 * @author a141890
 * @description This class supports the communication between Android and Server
 *              through HTTP Network
 * 
 */
public class MketSBBoardManager {
	// Singleton Member
	protected static MketSBBoardManager _singleton; 
	
	
	// Instance Members
	public ArrayList<MketSBArticle> articles;

	protected Context _context;
	protected String _retrieveUrl;
	
	
	public static MketSBBoardManager singleton(Context context, String retrieveUrl){
		if(_singleton == null){
			_singleton = new MketSBBoardManager(context, retrieveUrl);
		}
		return _singleton;
	}
	
	
	/**
	 * Constructor
	 * @param context
	 * @param retrieveUrl
	 */
	public MketSBBoardManager(Context context, String retrieveUrl) {
		this._context = context;
		this._retrieveUrl = retrieveUrl;
		if(articles == null){
			articles = new ArrayList<MketSBArticle>();
		}
	}
	
	public MketSBArticle getArticle(String serverId){
		if(articles == null){
			return null;
		}
		for(MketSBArticle article : articles){
			if(article.serverId.equals(serverId)){
				return article;
			}
		}
		return null;
	}
	
	public void retrieveArticlesFromServer(String key, String serverId, final Runnable callback){
		retrieveArticlesFromServer(key, serverId, 1, null, false, callback);
	}

	public void retrieveArticlesFromServer(String boardKey, String serverId, int page, HashMap<String, Object> data, final boolean refresh,
			final Runnable callback) {
		// Remove Different Board Key
		removeDifferentBoardKey(boardKey);
		
		// Build a Query String
		MketAsyncHttp http = new MketAsyncHttp();
		if (boardKey != null) {
			http.addQueryString("key", boardKey);
		}

		if (serverId == null) {
			http.addQueryString("page", page);
		} else {
			http.addQueryString("id", serverId);
		}

		if (data != null) {
			for (Entry<String, Object> entry : data.entrySet()) {
				String k = entry.getKey().toString();
				String v = entry.getValue().toString();
				http.addQueryString(k, v);
			}
		}

		http.listenPostExecute(new MketHttpCallback() {
			@Override
			public void callback(MketHttpClientResponse clientResponse) {
				MketTracker tracker = MketTracker.getSingleton();
				tracker.send("Mket Simple Baord - Retrieve Articles", clientResponse);
				
				if(articles == null){
					return;
				}
				

				if (clientResponse.code == HttpStatus.SC_OK) {
					try {
						JSONObject json = new JSONObject(clientResponse.content);
						JSONArray articlesJson = json.getJSONArray("articles");
						Log.d("CM", "MketSimpleBoardManager articles:" + articles);
						int size = articlesJson.length();
						for (int i = 0; i < size; i++) {

							MketSBArticle newArticle = MketSBArticle.create(articlesJson
									.getJSONObject(i));
							Log.d("CM", "MketSimpleBoardManager articlearticlearticle:" + newArticle);
							if (newArticle != null) {
								
								int articleTotalSize = articles.size();
								boolean alreadyHas = false;
								for(int j=0; j<articleTotalSize; j++){
									MketSBArticle existingArticle = articles.get(j);
									if(newArticle.serverId.equals(existingArticle.serverId)){
										existingArticle.update(newArticle);
										newArticle.destroy();
										alreadyHas = true;
										break;
									}
								}
								if(alreadyHas){							
									continue;
								}
								
								
								Log.d("CM", "MketSimpleBoardManager added:" + newArticle);
								if(refresh){
									articles.add(0, newArticle);
								}
								else{									
									articles.add(newArticle);
								}
							}
						}
					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}

				if (callback != null) {
					callback.run();
				}
			}
		});
		http.sendAsynchronously(_retrieveUrl);
	}
	
	public void removeDifferentBoardKey(String boardKey){
		if(articles != null){			
			int articleSize = articles.size();
			for(int i=0; i<articleSize; i++){
				MketSBArticle article = articles.get(i);
				if(!article.boardKey.equals(boardKey)){
					articles.remove(i);
				}
			}
		}
	}
	
	/**
	 * just locally change the comment count
	 * @param articleId
	 */
	public void increaseCommentCount(String articleId){
		if(articles != null){			
			int articleSize = articles.size();
			for(int i=0; i<articleSize; i++){
				MketSBArticle article = articles.get(i);
				if(article.serverId == articleId){
					article.commentCount += 1;
				}
			}
		}
	}
	
	/**
	 * just locally change the comment count
	 * @param articleId
	 */
	public void increaseViewCount(String articleId){
		if(articles != null){			
			int articleSize = articles.size();
			for(int i=0; i<articleSize; i++){
				MketSBArticle article = articles.get(i);
				if(article.serverId == articleId){
					article.viewCount += 1;
				}
			}
		}
	}

	public void destory() {
		_singleton = null;
		_context = null;
		_retrieveUrl = null;

		if (articles != null) {
			for (MketSBArticle article : articles) {
				article.destroy();
			}
			articles.clear();
			articles = null;
		}

	}

}
