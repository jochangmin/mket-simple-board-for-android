package biz.mket.simpleboard;

import java.util.ArrayList;

import android.R.integer;
import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import biz.mket.simpleboard.R;
import biz.mket.tools.MketTools;
import biz.mket.uploader.MketUploaderService;

public class MketSBCloudFragment extends MketSBBoardFragment {

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent intentData) {
		if (requestCode == _REQCODE_WRITE && resultCode == Activity.RESULT_OK) {
			String content = intentData.getStringExtra(MketSBWriteActivity.RETURN_CONTENT);
			ArrayList<String> imagePaths = intentData.getStringArrayListExtra(MketSBWriteActivity.RETURN_IMAGES);
			ArrayList<String> videoPaths = intentData.getStringArrayListExtra(MketSBWriteActivity.RETURN_VIDEOS);
			uploadToServer(content, imagePaths, videoPaths);

		}
	}

	public void uploadToServer(String content, ArrayList<String> imagePaths, ArrayList<String> videoPaths) {
		Log.d("CM", "MketSimpleBoardCloudFragment.uploadToServer " + content);
		if (_boardKey == null) {
			return;
		}
		if (content == null || content.length() < 1) {
			return;
		}

		int size = content.length();
		if (size > 50) {
			size = 50;
		}
		;
		content = content.trim();
		String[] lines = content.split(System.getProperty("line.separator"));
		String title = null;
		for (String line : lines) {
			line = line.trim();
			if (line.length() >= 1) {
				title = line;
				break;
			}
		}

		// init variables
		ArrayList<String> dataKeys = new ArrayList<String>();
		ArrayList<String> dataValues = new ArrayList<String>();
		ArrayList<String> queryKeys = new ArrayList<String>();
		ArrayList<String> queryValues = new ArrayList<String>();

		// add Key
		dataKeys.add("key");
		dataValues.add(_boardKey);

		// add user
		dataKeys.add("user");
		dataValues.add(_boardInterface.get_user_id());

		// add Title

		dataKeys.add("title");
		dataValues.add(title);

		// add content
		dataKeys.add("content");
		dataValues.add(content);

		// add query data
		queryKeys.add("key");
		queryValues.add(_boardKey);

		Intent serviceIntent = new Intent(getActivity(), MketUploaderService.class);
		serviceIntent.setData(Uri.parse(_settings.article_url));
		serviceIntent.putExtra(MketUploaderService.EXTRA_POST_DATA_KEYS, dataKeys);
		serviceIntent.putExtra(MketUploaderService.EXTRA_POST_DATA_VALUES, dataValues);
		serviceIntent.putExtra(MketUploaderService.EXTRA_DATA_QUERY_KEYS, queryKeys);
		serviceIntent.putExtra(MketUploaderService.EXTRA_DATA_QUERY_VALUES, queryValues);
		serviceIntent.putExtra(MketUploaderService.EXTRA_IMAGE_FILE_PATHS, imagePaths);
		serviceIntent.putExtra(MketUploaderService.EXTRA_VIDEO_FILE_PATHS, videoPaths);

		serviceIntent.putExtra(MketUploaderService.EXTRA_NOTIFICATION, true);
		serviceIntent.putExtra(MketUploaderService.EXTRA_NOTIFICATION_TITLE, "상품에 단 글을 업로드하고 있습니다");

		getActivity().startService(serviceIntent);

	}
}
