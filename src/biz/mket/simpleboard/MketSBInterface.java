package biz.mket.simpleboard;

import android.support.v4.app.FragmentActivity;
import android.view.View;
import biz.mket.image_cache.MketImageFetcher;
import biz.mket.simpleboard.article.MketSBArticle;

public interface MketSBInterface {
	// User
	public boolean is_authenticated();
	public void user_activity();
	public String get_user_id();
	public String get_username();
	public String get_email();
	public void login();
	
	// Mket Image Fetcher
	public MketImageFetcher getImageFetcher();
	
	// Write
	public void on_write_done(String content, String[] imagePathes, String[] videoPathes);
	
	// Read
	public void startDetailActivity(MketSBArticle article);
	
}
