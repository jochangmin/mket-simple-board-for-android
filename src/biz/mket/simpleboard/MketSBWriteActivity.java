package biz.mket.simpleboard;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;

import tv.showket.dialogs.SingleChoiceDialogFragment;
import tv.showket.views.ExpandableHeightGridView;
import android.content.ContentResolver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.database.Cursor;
import android.database.DataSetObserver;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.text.InputFilter;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.ViewTreeObserver;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.Toast;
import biz.mket.image_cache.MketImageResizer;
import biz.mket.simpleboard.settings.SimpleBoardSetting;
import biz.mket.storage.MketExternalFile;
import biz.mket.storage.MketExternalStorage;
import biz.mket.tools.MketMemoryUtils;
import biz.mket.tools.MketTools;

public class MketSBWriteActivity extends FragmentActivity {

	// Class Members
	public static final String EXTRA_BOARD_KEY = "board_key";
	public static final String EXTRA_DEFAULT_CONTENT = "default_content";
	public static final String EXTRA_COMMENT_ID = "comment_id";

	public static final String RETURN_CONTENT = "r_content";
	public static final String RETURN_ARTICLE_ID = "r_article_id";
	public static final String RETURN_IMAGES = "r_images";
	public static final String RETURN_VIDEOS = "r_videos";

	protected static final String PICTURE_TO_UPLOAD_DIRECTORY = "mket_simple_baord_pictures_to_upload";
	protected static final int REQ_CODE_PICK_IMAGE = 17;
	protected static final int REQ_CODE_PICK_VIDEO = 18;
	protected static final int REQ_CODE_TAKE_PICTURE = 19;
	protected static final int _MAXIMUM_VIDEO_COUNT = 1;

	// Instance Members
	protected String _boardKey;
	protected SimpleBoardSetting _settings;

	protected EditText _contentView;
	protected ExpandableHeightGridView _galleryView;
	protected Button _videoButton;

	protected String _articleId = null;
	protected int _contentMaximum = -1;
	protected int _imageMaximum = -1;

	protected int _thumbnailSize;
	protected int _thumbnailSpacing;
	protected ArrayList<String> _galleryArrayList = new ArrayList<String>();
	protected ArrayList<String> _imagePaths = new ArrayList<String>();
	protected HashMap<String, String> _videoPaths = new HashMap<String, String>();

	protected GalleryAdapter _galleryAdapter;
	protected GridView.LayoutParams _layoutParams;

	protected void onCreate(android.os.Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.simpleboard_write);

		// Remove notification bar
		this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

		// Get Extra Arguments
		Bundle args = getIntent().getExtras();
		String boardKey = args.getString(EXTRA_BOARD_KEY);
		String content = args.getString(EXTRA_DEFAULT_CONTENT);
		_articleId = args.getString(EXTRA_COMMENT_ID);

		if (boardKey == null) {
			boardKey = "default";
		}

		// Settings
		_settings = SimpleBoardSetting.getSetting(boardKey);
		
		_contentMaximum = _settings.document_max_content;
		_imageMaximum = _settings.document_max_images;
		
		// Views
		_galleryView = (ExpandableHeightGridView) findViewById(R.id.mket_simple_board_write_gallery);
		_videoButton = (Button) findViewById(R.id.simpleboard_write_videobutton);
		_thumbnailSize = getResources().getDimensionPixelSize(R.dimen.simpleboard_thumbnail_size);
		_thumbnailSpacing = getResources().getDimensionPixelSize(R.dimen.simpleboard_thumbnail_spacing);

		// init Title
		setTitle(_settings.document_titlebar);

		// init Edit Text for content
		_contentView = (EditText) findViewById(R.id.mket_simple_board_write_content);
		if (_contentMaximum >= 0) {
			_contentView.setFilters(new InputFilter[] { new InputFilter.LengthFilter(_contentMaximum) });
		}
		_contentView.setText(content);
		_contentView.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				Log.d("CM", "_contentView onClick");
				InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
				imm.showSoftInput(_contentView, InputMethodManager.SHOW_FORCED);
			}
		});

		_galleryAdapter = new GalleryAdapter();
		_galleryView.setExpanded(true);
		_galleryView.setAdapter(_galleryAdapter);
		// _galleryView.setOnItemClickListener(this);
		_galleryView.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
			@Override
			public void onGlobalLayout() {
				if (_galleryAdapter != null && _galleryView != null && _galleryAdapter.getNumColumns() == 0) {
					final int numColumns = (int) Math.floor(_galleryView.getWidth() / (_thumbnailSize + _thumbnailSpacing));
					if (numColumns > 0) {
						final int columnWidth = (_galleryView.getWidth() / numColumns) - _thumbnailSpacing;
						_galleryAdapter.setNumColumns(numColumns);
						_galleryAdapter.setItemHeight(columnWidth);

					}
				}
			}
		});

		// init Video Button
		if (_settings.video_available) {
			_videoButton.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					onclick_add_video(v);
				}
			});
		} else {
			_videoButton.setVisibility(View.GONE);
		}
	};

	@Override
	protected void onDestroy() {
		super.onDestroy();
		View rootView = getWindow().getDecorView().findViewById(android.R.id.content);
		MketMemoryUtils.recursiveRecycle(rootView, false);

		if (_galleryArrayList != null) {
			_galleryArrayList.clear();
			_galleryArrayList = null;
		}

		if (_imagePaths != null) {
			_imagePaths.clear();
			_imagePaths = null;
		}

		if (_videoPaths != null) {
			_videoPaths.clear();
			_videoPaths = null;
		}

		if (_galleryView != null) {
			_galleryView = null;
		}

		_settings = null;
		_contentView = null;
		_contentMaximum = 0;

		_contentView = null;
		_galleryView = null;
		_articleId = null;
		_galleryAdapter = null;
		_layoutParams = null;

		System.gc();
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		Log.d("CM", "onKeyDown keyCode :" + String.valueOf(keyCode));
		Log.d("CM", "onKeyDown event :" + String.valueOf(event));
		Log.d("CM", "onKeyDown event.getAction :" + String.valueOf(event.getAction()));
		if (event.getKeyCode() == KeyEvent.KEYCODE_BACK) {
			MketTools.hideKeyboard(this, _contentView);
		}
		return super.onKeyDown(keyCode, event);

	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent returnedIntent) {
		super.onActivityResult(requestCode, resultCode, returnedIntent);

		// check if the taken or picked images exist, if not exists remove the
		// image.
		if (requestCode == REQ_CODE_PICK_IMAGE || requestCode == REQ_CODE_TAKE_PICTURE) {
			for (int i = 0; i < _galleryArrayList.size(); i++) {
				File file = new File(_galleryArrayList.get(i));
				if (!file.exists()) {
					_galleryArrayList.remove(i);
				}
			}
		}

		if (requestCode == REQ_CODE_PICK_IMAGE && resultCode == RESULT_OK) {
			Uri pictureUri = returnedIntent.getData();
			String[] filePathColumn = { MediaStore.Images.Media.DATA };
			Cursor cursor = getContentResolver().query(pictureUri, filePathColumn, null, null, null);
			cursor.moveToFirst();
			int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
			// /storage/sdcard0/DCIM/Camera/1367664165879.jpg
			String filePath = cursor.getString(columnIndex);
			cursor.close();

			if (!_galleryArrayList.contains(filePath)) {
				_galleryArrayList.add(filePath);
				_imagePaths.add(filePath);
			}
		} else if (requestCode == REQ_CODE_PICK_VIDEO && resultCode == RESULT_OK) {
			Uri videoUri = returnedIntent.getData();

			String[] filePathColumn = { MediaStore.Video.Media._ID, MediaStore.Images.Media.DATA };
			Cursor cursor = getContentResolver().query(videoUri, filePathColumn, null, null, null);
			cursor.moveToFirst();

			long fileId = cursor.getLong(cursor.getColumnIndex(MediaStore.Video.Media._ID));
			String filePath = cursor.getString(cursor.getColumnIndex(MediaStore.Images.Media.DATA));

			ContentResolver crThumb = getContentResolver();
			Bitmap thumbnailBitmap = MediaStore.Video.Thumbnails
					.getThumbnail(crThumb, fileId, MediaStore.Video.Thumbnails.MICRO_KIND, null);

			MketExternalStorage mketStorage = new MketExternalStorage(getApplicationContext(), MketExternalStorage.MODE_EXTERNAL_CACHE);
			MketExternalFile mketFile = mketStorage.storeSynchronously(PICTURE_TO_UPLOAD_DIRECTORY + "/" + fileId, thumbnailBitmap);

			if (!_galleryArrayList.contains(mketFile.getAbsolutePath())) {
				_galleryArrayList.add(mketFile.getAbsolutePath());
				_videoPaths.put(mketFile.getAbsolutePath(), filePath);
			}

			Log.d("CM", "onActivityResult video fileId:" + String.valueOf(fileId));
			Log.d("CM", "onActivityResult video filePath:" + String.valueOf(filePath));
			Log.d("CM", "onActivityResult video image Path:" + String.valueOf(mketFile.getAbsolutePath()));
		}

		_galleryAdapter.notifyDataSetChanged();
	}

	public void onclick_add_picture(View v) {
		MketTools.hideKeyboard(this, _contentView);

		if (_imagePaths.size() >= _imageMaximum) {
			Toast.makeText(this, "이미지 업로드 제한 갯수를 넘었습니다", Toast.LENGTH_SHORT).show();
			return;
		}

		String[] items = { "앨범에서 선택하기", "집접 사진찍기" };
		SingleChoiceDialogFragment dialogFragment = SingleChoiceDialogFragment.newInstance("프로필 사진 바꾸기", items, new OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				if (which == 0) {

					Intent i = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
					startActivityForResult(i, REQ_CODE_PICK_IMAGE);
					dialog.dismiss();
				} else if (which == 1) {
					File imageFile = MketExternalFile.createCachedFile(MketSBWriteActivity.this, PICTURE_TO_UPLOAD_DIRECTORY, "jpg");
					MketExternalFile.createParentDirectory(imageFile);

					_galleryArrayList.add(imageFile.getAbsolutePath());
					_imagePaths.add(imageFile.getAbsolutePath());

					Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
					takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(imageFile));
					startActivityForResult(takePictureIntent, REQ_CODE_TAKE_PICTURE);
					dialog.dismiss();
				}
			}
		});
		dialogFragment.setTitle("사진 추가 하기");
		FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
		dialogFragment.show(ft, "change_profiel_picture");
	}

	public void onclick_add_video(View v) {
		MketTools.hideKeyboard(this, v);
		if (_videoPaths.size() >= _MAXIMUM_VIDEO_COUNT) {
			Toast.makeText(this, "동영상 업로드 제한 갯수를 넘었습니다", Toast.LENGTH_SHORT).show();
			return;
		}

		String[] items = { "앨범에서 선택하기" };
		SingleChoiceDialogFragment dialogFragment = SingleChoiceDialogFragment.newInstance("프로필 사진 바꾸기", items, new OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				if (which == 0) {
					Intent videoPickIntent = new Intent(Intent.ACTION_GET_CONTENT);
					// comma-separated MIME types
					videoPickIntent.setType("video/*");
					startActivityForResult(videoPickIntent, REQ_CODE_PICK_VIDEO);
					dialog.dismiss();
				} else if (which == 1) {
					File imageFile = MketExternalFile.createCachedFile(MketSBWriteActivity.this, PICTURE_TO_UPLOAD_DIRECTORY, "jpg");
					MketExternalFile.createParentDirectory(imageFile);

					_galleryArrayList.add(imageFile.getAbsolutePath());
					Intent takePictureIntent = new Intent(MediaStore.ACTION_VIDEO_CAPTURE);
					takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(imageFile));
					startActivityForResult(takePictureIntent, REQ_CODE_TAKE_PICTURE);
					dialog.dismiss();
				}
			}
		});
		dialogFragment.setTitle("동영상 추가 하기");
		FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
		dialogFragment.show(ft, "change_profile_picture");
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		super.onCreateOptionsMenu(menu);

		MenuInflater menuInflater = getMenuInflater();
		menuInflater.inflate(R.menu.mket_simple_board_write_menu, menu);

		return true;
	}

	@Override
	public boolean onMenuItemSelected(int featureId, MenuItem item) {
		super.onMenuItemSelected(featureId, item);
		MketTools.hideKeyboard(this, _contentView);

		// when clicking finish button
		if (featureId == 0) {
			returnWrittenContent();
		}
		return true;
	}

	public void returnWrittenContent() {
		ArrayList<String> videoPathArrayList = new ArrayList<String>();
		for (String value : _videoPaths.values()) {
			videoPathArrayList.add(value);
		}

		String content = _contentView.getText().toString();

		// validate content
		if (content == null || content.length() <= 0) {
			Toast.makeText(getApplicationContext(), "완료하시려면 글을 남겨주세요^^", Toast.LENGTH_SHORT).show();
			return;
		}

		Intent intent = new Intent();
		intent.putExtra(RETURN_CONTENT, content);
		intent.putExtra(RETURN_ARTICLE_ID, _articleId);
		intent.putExtra(RETURN_IMAGES, _imagePaths);
		intent.putExtra(RETURN_VIDEOS, videoPathArrayList);
		setResult(RESULT_OK, intent);

		Log.d("CM", "returnWrittenContent content" + String.valueOf(content));
		Log.d("CM", "returnWrittenContent _articleId" + String.valueOf(_articleId));
		Log.d("CM", "returnWrittenContent _imagePaths" + String.valueOf(_imagePaths));
		Log.d("CM", "returnWrittenContent videoPathArrayList" + String.valueOf(videoPathArrayList));

		finish();
	}

	public class GalleryAdapter extends BaseAdapter {
		public int itemHeight = 0;
		public int _columnCount = 0;
		public GridView.LayoutParams layoutParams;

		public GalleryAdapter() {
			layoutParams = new GridView.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);
		}

		@Override
		public int getCount() {
			return _galleryArrayList.size();
		}

		@Override
		public Object getItem(int position) {
			return _galleryArrayList.get(position);
		}

		@Override
		public long getItemId(int position) {
			return position;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			// Now handle the main ImageView thumbnails
			final ImageView imageView;
			if (convertView == null) { // if it's not recycled, instantiate and
										// initialize
				imageView = new ImageView(MketSBWriteActivity.this);
				imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
				imageView.setLayoutParams(layoutParams);
			} else { // Otherwise re-use the converted view
				imageView = (ImageView) convertView;
			}

			// Check the height matches our calculated column width
			if (imageView.getLayoutParams().height != itemHeight) {
				imageView.setLayoutParams(layoutParams);
			}

			// Finally load the image asynchronously into the ImageView, this
			// also takes care of setting a placeholder image while the
			// background thread runs

			Bitmap bitmap = MketImageResizer.decodeSampledBitmapFromFile(_galleryArrayList.get(position), _thumbnailSize, _thumbnailSize,
					true, true);
			// Bitmap bitmap =
			// MketImageResizer.decodeSampledBitmapFromFile(filePath,
			// _imageView.getWidth(),
			// _imageView.getWidth(), true);
			BitmapDrawable drawable = new BitmapDrawable(bitmap);
			imageView.setBackgroundDrawable(drawable);

			return imageView;
		}

		@Override
		public void unregisterDataSetObserver(DataSetObserver observer) {
			if (observer != null) {
				super.unregisterDataSetObserver(observer);
			}
		}

		@Override
		public boolean hasStableIds() {
			return true;
		}

		public void setItemHeight(int height) {
			if (height == itemHeight) {
				return;
			}
			itemHeight = height;
			layoutParams = new GridView.LayoutParams(LayoutParams.MATCH_PARENT, itemHeight);
			// imageFetcher.setImageSize(height);
			notifyDataSetChanged();
		}

		public void setNumColumns(int columnCount) {
			_columnCount = columnCount;
		}

		public int getNumColumns() {
			return _columnCount;
		}

	}
}
