package biz.mket.simpleboard.settings;

import java.util.HashMap;

import android.R.integer;
import android.graphics.drawable.Drawable;
import biz.mket.simpleboard.R;

public class SimpleBoardSetting {
	public static HashMap<String, SimpleBoardSetting> settings = new HashMap<String, SimpleBoardSetting>();

	// Key
	public String key = "default";

	// URL
	public String article_url = ""; // both retrieving and uploading are used.  
	public String comment_url = ""; // both retrieving and uploading are used. 
	
	// Colors and Drawables
	public Drawable background;

	// Write Button
	public int write_text = R.string.simpleboard_write_text;
	public int write_text_color = R.color.simpleboard_write_text;
	public int write_background = R.color.simpleboard_write_background;
	public boolean writable = true;
	
	// Write Activity
	public String document_titlebar = "글 쓰 기";
	public String document_hint = "";
	public int document_max_content = 8000;
	public int document_max_images = 10;

	// Photo
	public boolean picture_available = true;

	// Video
	public boolean video_available = false;

	// Constructor
	public SimpleBoardSetting() {
	}

	public SimpleBoardSetting(String key) {
		this.key = key;
	}

	public void changKey(String key) {
		this.key = key;
	}
	
	// Clone
	public SimpleBoardSetting clone(){
		return clone(key);
	}
	
	public SimpleBoardSetting clone(String anotherKey){
		SimpleBoardSetting _new = new SimpleBoardSetting(anotherKey);
		
		// URLs
		_new.article_url = article_url;
		_new.comment_url = comment_url;
		
		// Write Button
		_new.write_text = write_text;
		_new.write_text_color = write_text_color;
		_new.write_background = write_background;
		_new.writable = writable;
		
		// Photo
		_new.picture_available = picture_available;
		
		// Video
		_new.video_available = video_available;
		
		return _new;
		
	}

	/**
	 * Destroy
	 */
	public void destroy() {
		key = null;
	}
	

	// Manager
	public static void add(SimpleBoardSetting setting) {
		if (settings.containsKey(setting.key)) {
			settings.get(setting.key).destroy();
			settings.remove(setting.key);
		}
		settings.put(setting.key, setting);
	}

	public static SimpleBoardSetting getSetting(String key) {
		if (settings.containsKey(key)) {
			return settings.get(key);
		}
		return new SimpleBoardSetting();
	}

}
